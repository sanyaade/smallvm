# MicroBlocks translation file
# Last updated: Jan  09  2023
#########################
# Blocks and categories #
#########################

Output
Εξόδοι

set user LED _
όρισε το LED χρήστη _

say _
πες _

graph _
σχεδίασε _

Input
Εισόδοι

button A
κουμπί Α

button B
κουμπί Β

microseconds
εκατομμυριοστά δευτερολέπτου

milliseconds
χιλιοστά δευτερολέπτου

board type
είδος κάρτας

Pins
Ακροδέκτες

read digital pin _
διάβασε το ψηφιακό ακροδέκτη _

read analog pin _
διάβασε τον αναλογικό ακροδέκτη _

set digital pin _ to _
όρισε το ψηφιακό ακροδέκτη _ σε _

set pin _ to _
όρισε τον ακροδέκτη _ σε _

analog pins
αναλογικοί ακροδέκτες

digital pins
ψηφιακοί ακροδέκτες 

Control
Έλεγχος Ροής

when started
όταν ξεκινά

when button _ pressed
όταν το κουμπί _ πατηθεί

forever _
Για πάντα επανάλαβε _

repeat _ _
επανάλαβε _ _

wait _ millisecs
περίμενε, _ χιλιοστά δευτερολέπτου

if _ _
Αν _ _

else if _ _
διαφορετικά αν _ _

if _ _ else _
αν _ _ διαφορετικά _

else
διαφορετικά

when _
όταν _

wait until _
περίμενε μέχρι _

wait _ microsecs
περίμενε _ εκατομμυριοστά δευτερολέπτου

return _
επιστροφή _

when _ received
όταν _ ληφθεί

broadcast _
μετάδωσε _

comment _
σχόλιο _

for _ in _ _
για _ μέσα σε _ _

repeat until _ _
επανάλαβε μέχρι _ _

stop this task
σταμάτα αυτό το σενάριο

stop other tasks
σταμάτα τα άλλα σενάρια

stop all
σταμάτα όλα τα σενάρια

last message
τελευταίο μήνυμα

Operators
Τελεστές

_ mod _
_ ακέραιο υπόλοιπο _

abs _
απόλυτο _

random _ to _
τυχαίο _ ως _

not _
αντίθετο _

_ and _
_ και _

and _
και _

_ or _
_ ή _

or _
ή _

_ is a _
_ είναι ένα _

boolean
boolean

number
αριθμός

string
συμβολοσειρά

list
λίστα

Advanced:
Προχωρημένα:

hex _
δεκαεξαδικό _

Variables
Μεταβλητές

# Buttons on top of "Variables" category

Add a variable
Πρόσθεσε μια μεταβλητή

Delete a variable
Διάγραψε μια μεταβλητή

# New variable dialog

New variable name?
Όνομα νέας μεταβλητής?

set _ to _
όρισε _ σε _

change _ by _
άλλαξε _ ανά _

initialize local _ to _
αρχική τιμή τοπικής μεταβλητής _ σε _

Data
Δεδομένα

list
λίστα

length of _
μήκος του _

item _ of _
στοιχείο _ του _

replace item _ of list _ with _
αντικατέστησε στοιχείο _ της λίστας _ με _

delete item _ of list _
διάγραψε το στοιχείο _ της λίστας _

add _ to list _
πρόσθεσε _ στην λίστα _

join _ _
ένωσε _ _

copy _ from _
αντίγραψε _ από _

to _
προς _

find _ in _
βρες _ μέσα _

starting at _
ξεκινά από _

join items of list _
ένωσε τα στοιχεία της λίστας _

separator _
διαχωριστικό _

unicode _ of _
unicode _ του _

string from unicode _
συμβολοσείρα από unicode _

new list length _
μήκος νέου καταλόγου _

new byte array _
νέος byte πίνακας _

free memory
ελευθέρωσε μνήμη

My Blocks
Τα Mπλοκ μου

# Buttons on top of "My Blocks" category

Add a command block
Πρόσθεσε μπλοκ εντολής

Add a reporter block
Πρόσθεσε μπλοκ αναφοράς

# Make a block dialog

Enter function name:
Συμπλήρωσε το όνομα της λειτουργίας:

Comm
Επικοινωνία 

i2c get device _ register _
i2c πάρε από συσκευή _ καταχωρητή _

i2c set device _ register _ to _
i2c όρισε στην συσκευή _ καταχωρητή _ σε _

i2c device _ read list _
i2c συσκευή _ διάβασε λίστα _

i2c device _ write list _
i2c συσκευή _ γράψε λίστα _

spi send _
spi στείλε _

spi receive
spi λάβε

print _
τύπωσε _

no op
καμμία εντολή

ignore
αγνόησε 

##############
# Primitives #
##############

# These are all mostly hidden from end users

draw shape _ at x _ y _
σχεδίασε σχήμα _ στο x _ y _

shape for letter _
σχήμα για γράμμα _

send NeoPixel rgb _
στείλε στο NeoPixel κπμ _

has tone support
έχει τονική υποστήριξη

play tone pin _ frequency _
παίξε τόνο στον ακροδέκτη _ συχνότητα _

has WiFi support
διαθέτει υποστήριξη WIFI

start WiFi _ password _
ξεκίνα WIFI _ με κωδικό _

stop WiFi
σταμάτα WIFI

WiFi status
WIFI κατάσταση

my IP address
η IP διεύθυνση μου 

radio send number _
ράδιο στείλε αριθμό _ 

radio send string _
ράδιο στείλε σειρά χαρακτήρων

radio send pair _ = _
ράδιο στείλε ζευγάρι _ =_

radio message received?
παραλήφθηκε το μήνυμα ραδίου

radio last number
ράδιο τελευταίος αριθμός

radio last string
ράδιο τελευταία σειρά χαρακτήρων

radio last message type
ράδιο τύπος τελευταίου μηνύματος

radio set group _
ράδιο ορισμός ομάδας _ 

radio set channel (0-83) _
ράδιο όρισε κανάλι σε (0-83) _

radio set power (0-7) _
ράδιο όρισε ισχύ σε (0-7) _

radio last signal strength
ράδιο δύναμη τελευταίου σήματος

radio receive packet _
ράδιο λήψη πακέτου _ 

radio send packet _
ράδιο αποστολή πακέτου _ 

disable radio
Απενεργοποίηση ραδίου

#############
# Libraries #
#############

Basic Sensors
Βασικοί Αισθητήρες

tilt x
κλίση x

tilt y
κλίση y

tilt z
κλίση z

acceleration
επιτάχυνση

light level
Μέτρηση φωτός

temperature (°C)
Θερμοκρασία (°C)

####
# NeoPixel library

NeoPixel
--MISSING--

set NeoPixels _ _ _ _ _ _ _ _ _ _
όρισε NeoPixels _ _ _ _ _ _ _ _ _ _

clear NeoPixels
καθάρισε NeoPixels

set NeoPixel _ color _
όρισε NeoPixel _ χρώμα _

set all NeoPixels color _
όρισε όλα τα NeoPixels σε χρώμα _

rotate NeoPixels by _
στρίψε τα NeoPixels κατά _

color r _ g _ b _ (0-255)
χρώμα κ _ π _ μ _ (0-255)

random color
τυχαίο χρώμα

attach _ LED NeoPixel strip to pin _
σύνδεσε _ LED NeoPixel strip στον ακροδέκτη _

has white _
έχει άσπρο _

PIR
PIR

PIR at pin _ detected movement
PIR στον ακροδέκτη _ ανίχνευσε κίνηση

####
# Citilab ED1 board libraries

ED1 Stepper Motor
ED1 Μότερ Βήμάτος

move motor _ _ steps _
μοτερ  _ _ βήματα _

move motor 1 _ and motor 2 _ _ steps
μοτερ 1 κινήθου _ και μοτερ 2 κινήθου _ _ βήματα

move motor _ angle _ °
μοτέρ _ στρίψε γωνία _ °

move motor _ _ complete turns
μοτέρ στρίψε _ _ στροφές

stop steppers
σταμάτα τα βηματικά μοτέρ 

clockwise
δεξιόστροφα

counter-clockwise
αριστερόστροφα

ED1 Buttons
ED1 Κομβία

button OK
 κουμπί OK

button X
 κουμπί X

button up
 κουμπί πάνω

button down
 κουμπί κάτω

button left
 κουμπί αριστερά

button right
 κουμπί δεξιά

capacitive sensor _
χωρητικός αισθητήρας _

set capacitive threshold to _
ορίσε όριο του χωρητικού σε_

####
# BirdBrain Technologies libraries

Hummingbird LED _ _ %
--MISSING--

Hummingbird Tri-LED _ R _ % G _ % B _ %
--MISSING--

Hummingbird Position Servo _ _ °
Hummingbird θέση Servo _ _ °

Hummingbird Rotation Servo _ _ %
Hummingbird Περιστροφή Servo _ _ %

Hummingbird _ _
--MISSING--

Hummingbird Battery (mV)
Hummingbird μπαταρία (mV)

Light
Φως

Distance (cm)
Απόσταση (εκ)

Dial
--MISSING--

Sound
Ήχος

Other
Άλλα

Finch Ράμφος Red _ Green _ Blue _
Finch Beak Κόκκινο _ Πράσινο _ Μπλέ _

Finch Tail _ Red _ Green _ Blue _
Finch Ουρά Κόκκινο _ Πράσινο _ Μπλέ _

Finch Move _ _ cm at _ %
Finch Προχώρα _ _ εκ  με  _ %

Finch Turn _ _ ° at _ %
Finch Στρίψε _ _ ° με _ %

Finch Wheels L _ % R _ %
Finch Τροχοί Α _ % Δ _ %

Finch Stop
Finch Σταμάτα

Finch Distance (cm)
Finch Απόσταση (εκ)

Finch _ Light
Finch _ Φως

Finch _ Line
Finch _ Απόσταση

Finch Reset Encoders
Finch Μηδένισε  Encoders

Finch _ Encoder
--MISSING--

Finch Accelerometer _
Finch Επιταχυνσιόμετρο _

Finch Battery
Finch Μπαταρία

All
Όλα

Forward
Μπροστά

Backward
Πίσω

Right
Δεξιά

Left
Αριστερά

Beak Up
Ράμφος Πάνω

Beak Down
Ράμφος Κάτω

Tilt Left
Κλίνε Αριστερά

Tilt Right
Κλίνε Δεξιά

Level
Ίσιωσε

Upside Down
Ανάποδα

x
--MISSING--

y
--MISSING--

z
--MISSING--

strength
δύναμη

####
# Ultrasound distance library

distance (cm) trigger _ echo _
απόσταση (εκ) ενεργοποίηση _ ηχώ _

####
# Infrared remote library

IR Remote
IR Τηλεχειριστήριο

receive IR code
λήψη κωδικού IR

receive IR code from device _
λήψη κωδικού IR από τη συσκευή _

test IR
δοκιμή IR

attach IR receiver to pin _
επισυνάψτε δέκτη IR στον ακροδέκτη _

####
# Radio comm library
# Allows micro:bit boards to exchange messages
# All of its blocks are primitive (see "Primitives" section)

Radio
--MISSING--

####
# Text scrolling library
# Scrolls text on 5x5 LED displays and simulated ones

Scrolling
κύλιση

scroll text _
κύλιση κειμένου _

scroll number _
κύλιση αριθμού _ 

pausing _ ms
παύση _ χιλιοστά δευτερολέπτου

stop scrolling
σταμάτημα κύλισης

####
# Servo motor library

Servo
Σέρβο μοτέρ

set servo _ to _ degrees (-90 to 90)
ορίσε servo από _ έως _ μοίρες (-90 έως 90)

set servo _ to speed _ (-100 to 100)
ορίσε servo _ στην ταχύτητα _ (-100 έως 100)

stop servo _
σταμάτησε το σέρβο _

####
# 5x5 LED display library
# Supports the micro:bit display, but also simulated 5x5 displays on boards
# with differently sized LED arrays, NeoPixel arrays or TFT displays

LED Display
Οθόνη LED

display _
οθόνη _

clear display
καθάρισμα Οθόνης

plot x _ y _
σχεδιάσε x _ y _

unplot x _ y _
καθάρισε x _ y _

display character _
εμφάνισε χαρακτήρα _

####
# TFT display library

enable TFT _
ενεργοποίησε TFT _

TFT width
TFT πλάτος

TFT height
TFT ύψος

set TFT pixel x _ y _ to _
Όρισε TFT εικονοκύταρο x _ y _ σε _

draw line on TFT from x _ y _ to x _ y _ color _
σχεδιάσε γραμμή  στο TFT από x _ y _ στο x _ y _ χρώμα _

draw rectangle on TFT at x _ y _ width _ height _ color _
σχεδίασε τετράγωνο στο TFT στο x _ y _ πλάτος _ ύψος _ χρώμα _

draw rounded rectangle on TFT at x _ y _ width _ height _ radius _ color _
σχεδίασε στρογγυλεμένο τετράγωνο στο TFT στο x _ y _ πλάτος _ ύψος _  ακτίνα _ χρώμα _

draw circle on TFT at x _ y _ radius _ color _
σχεδιάσε κύκλο στο  TFT στο x _ y _ ακτίνα _ χρώμα _

draw triangle on TFT at x _ y _ , x _ y _ , x _ y _ color _
σχεδιάσε τρίγωνο στο TFT στο x _ y _ , x _ y _ , x _ y _ χρώμα _

filled _
Γέμιση _

write _ on TFT at x _ y _ color _
γράψε _ στο TFT στο x _ y _ χρώμα _

scale _ wrap _
κλίμακα _ αναδίπλωση _

####
# Tone library
# Generates music tones on buzzers

Tone
Τόνος

attach buzzer to pin _
επισυνάψε το βομβητή στον ακροδέκτη _

play note _ octave _ for _ ms
παίξε νότα _ οκτάβα _ για _ χδ

play frequency _ for _ ms
παίξε συχνότητα _ για _ χδ

play midi key _ for _ ms
παίξε πλήκτρο midi _ για _ ms

####
# Turtle geometry library

Turtle
Χελώνα 

home
Σπίτι

move _
προχώρα _

turn _ degrees
στρίψε _ μοίρες

turn _ / _ of circle
στρίψε _ / _ του κύκλου

pen down
πέννα κάτω

pen up
πέννα πάνω

set pen color to _
όρισε την πέννα στο χρώμα _

set pen to random color
όρισε την πέννα σε τυχαίο χρώμα

fill display with _
γέμισε την οθόνη με _

go to x _ y _
προχώρα στο x _ y _

point in direction _
σημείο στην κατεύθυνση _

####
# File system library

Files
Αρχεία

open file _
άνοιξε αρχείο _

close file _
κλείσε αρχείο _

delete file _
διάγραψε αρχείο _

append line _ to file _
πρόσθεσε γραμμή _ στο αρχείο _

append bytes _ to file _
πρόσθεσε bytes _ στο αρχείο _

end of file _
τέλος του αρχείου _

next line of file _
επόμενη γραμμή του αρχείου _

next _ bytes of file _
επόμενα _ bytes του αρχείου _

starting at _
ξεκινά από _

file names
όνομα αρχείων

size of file _
μέγεθος του αρχείου _

file system info
πληροφορίες για αρχεία συστήματος

####
# WiFi library

WiFi
--MISSING--

wifi connect to _ password _ try _ times
wifi ενώθου στο _ κωδικός _ προσπάθησε _ φορές

wifi create hotspot _ password _
wifi δημιούργησε hotspot _ κωδικός _

IP address
IP διεύθυνση

####
# Motion library
# Counts steps and detects motion via the internal accelerometer

Motion
Κίνηση

motion
κίνηση

start step counter
ξεκίνα μετρητή βημάτων

step count
μέτρησε βήματα

clear step count
Μηδένισε των μετρητή βημάτων

set step threshold _ (0-50)
όρισε κατώφλι βημάτων _ (0-50)

####
# Button Events library

Button Events
κουμπί Συμβάντα

button _ double pressed
κουμπί _ διπλό πάτημα

button _ long pressed
κουμπί _ μακρύ πάτημα

button _ pressed
κουμπί _ πατήθηκε

####
# Calliope board library

Calliope set LED red _ green _ blue _
--MISSING--

Calliope set speaker _
--MISSING--

Calliope loudness
--MISSING--

####
# Circuit Playground Express board library

Circuit Playground set speaker _
Circuit Playground όρισε το μεγάφωνο _

Circuit Playground slide switch
Circuit Playground κουμπί κύλισης 

####
# DotStar LED library

attach _ DotStar LEDs to data pin _ clock pin _
--MISSING--

set all DotStar LEDs to r _ g _ b _
--MISSING--

set DotStar LED _ to r _ g _ b _
--MISSING--

set DotStar brightness _
--MISSING--

####
# BME280 environmental sensor

bme280 connected
bme280 ενώθηκε

bmp280 connected
-bmp280 ενώθηκε

bmx280 temperature
bmx280 θερμοκρασία

bmx280 pressure
bmx280 πίεση

bme280 humidity
bme280 υγρασία

####
# TCS34725 color sensor

TCS34725 connected
TCS34725 συνδέθηκε

TCS34725 rgb
TCS34725 κπμ

color _ name
χρώμα _ όνομα

####
# DHT11 environmental sensor

temperature (Celsius) DHT11 pin _
θερμοκρασία (Κελσίου) DHT11 ακροδέκτης _

humidity DHT11 pin _
υγρασία DHT11 ακροδέκτης _ 

temperature (Celsius) DHT22 pin _
θερμοκρασία (Κελσίου) DHT22 ακροδέκτης _

humidity DHT22 pin _
υγρασία DHT22 ακροδέκτης _ 

####
# PN532 RFID reader

read PN532 RFID
διάβασε PN532 RFID

RFID _ = _
--MISSING--

get PN532 firmware version
διάβασε PN532 έκδοση firmware

####
# HTTP libraries

HTTP client
HTTP πελάτης

_ data _ to http:// _
_ δεδομένα _ στο http://_

HTTP server
HTTP εξυπηρετητής

start HTTP server
ξεκίνα τον HTTP εξυπηρετητή

HTTP server request
HTTP εξυπηρετητή αίτηση

respond _ to HTTP request
απάντηση _ σε HTTP αίτηση

with body _
με σώμα _

and headers _
και επικεφαλίδες _

body of request _
σώμα της αίτησης

path of request _
διαδρομή αίτησης _

method of request _
μέθοδος της αίτησης _

####
# Web Things library

Web Thing
Αντικείμενο Δικτύου

set thing name to _
όρισε το όνομα του αντικειμένου σε _

set thing capability to _
όρισε την ικανότητα του αντικειμένου σε _

set boolean property _ title _ @Type _
όρισε δυαδική ιδιότητα _ τίτλος _ @Τύπος

set string property _ title _ @Type _
όρισε κειμένου ιδιότητα _ τίτλος _ @Τύπος _

set number property _ title _ @Type _
όρισε αριθμητική ιδιότητα _ τίτλος _ @Τύπος _

set number property _ title _ min _ max _ @Type _
όρισε αριθμητική ιδιότητα _ τίτλος _ ελάχιστο _ μέγιστο _ @Τύπος _

read only _
διάβασε μόνο _

register event _ type _
καταχώρησε γεγονός _ τύπος _

start WebThing server
ξεκίνα εξυπηρετητή WebThing

trigger event _
ενεργοποίηση γεγονότος _

thing description JSON
περιγραφή αντικειμένου JSON

properties JSON
ιδιότητες JSON

event definitions JSON
ορισμοί γεγονότων JSON

events JSON
γεγονότα JSON

##################
# MicroBlocks UI #
##################

# buttons, error & info messages, dialog boxes, etc

New
Νέο

Open
Άνοιξε

Open from board
Άνοιξε από την κάρτα

Information
Πληροφορία

Plug in the board.
Σύνδεσε την κάρτα 

Reading project from board...
Διαβάζει το έργο από την κάρτα …

Loading project...
Φορτώνει το έργο…

Found a newer version of
Βρέθηκε πιο νέα έκδοση από

Do you want me to update the one in the project?
Επιθυμείς να ανανεώσω αυτό του έργου;

Save
Αποθήκευσε

Connect
Συνδέσου

disconnect
Αποσυνδέσου

Serial port:
Σειριακή θύρα:

other...
άλλα…

none
κανένα

Port name?
Όνομα θύρας;

Board type:
Τύπος κάρτας:

Select board:
Επιλογή κάρτας:

Could not read:
Δεν μπορεί να διαβάσει:

by
από

Created with GP
Δημιουργήθηκε με GP

More info at http://microblocks.fun
Περισσότερες πληροφορίες στο http://microblocks.fun

Function "
Λειτουργία “

" is too large to send to board.
“ είναι πολύ μεγάλο για να σταλεί στην κάρτα

Script is too large to send to board.
Το κείμενο είναι πολύ μεγάλο για να σταλεί στην κάρτα

Use "Connect" button to connect to a MicroBlocks device.
Χρησιμοποίησε “Συνδέσου” για να συνδεθείς στη συσκευή MicroBlocks.

No boards found; is your board plugged in?
Δεν βρέθηκαν κάρτες, είναι συνδεδεμένη η κάρτα σου;

For AdaFruit boards, double-click reset button and try again.
Για τις κάρτες Adafruit, διπλό κλίκ στο κουμπί επαναφοράς (reset)και δοκίμασε ξανά.

The board is not responding.
Η κάρτα δεν ανταποκρίνεται.

Try to Install MicroBlocks on the board?
Δοκίμασες να εγκαταστήσεις το Microblocks στην κάρτα;

The MicroBlocks in your board is not current
Το Microblocks στην κάρτα σου δεν είναι το τελευταίο

Try to update MicroBlocks on the board?
Προσπάθησες να εγκαταστήσεις το Microblocks στην κάρτα;

Stop
Σταμάτα

Start
Ξεκίνα

Quit MicroBlocks?
Να σταματήσω το MicroBlocks;

Discard current project?
Να μην αποθηκεύσω το τρέχων έργο;

clean up
καθαρισμός

arrange scripts
Συγήρισε τα κείμενα

undrop (ctrl-Z)
--MISSING--

copy all scripts to clipboard
Αντίγραψε όλα τα κείμενα στο πρόχειρο

paste all scripts
Επικόλησε όλα τα κείμενα

paste script
Επικόλησε κείμενο

save a picture of all scripts
αποθήκευσε φωτογραφία όλων των κειμένων

about...
σχετικά…

virtual machine version
εικονική μηχανή έκδοση 

update firmware on board
επικαιροποίησε το firmware στην κάρτα

show data graph
εμφάνισε την γραφική παράσταση

set serial delay
όρισε την σειριακή καθυστέρηση

firmware version
έκδοση firmware

start WebThing server
ξεκίνα εξυπηρετητή WebThing

stop WebThing server
σταμάτα εξυπηρετητή WebThing

HTTP Server
HTTP εξυπηρετητής

MicroBlocks HTTP Server listening on port 6473
MicroBlocks HTTP εξυπηρετητής ακούει στην θύρα 6473

disable autoloading board libraries
απενεργοποίηση αυτόματης φόρτωσης των βιβλιοθηκών της κάρτας

enable autoloading board libraries
ενεργοποίηση της αυτόματης φόρτωσης των βιβλιοθηκών της κάρτας

enable PlugShare when project empty
ενεργοποίηση PlugShare όταν το έργο είναι άδειο

disable PlugShare when project empty
απενεργοποίηση PlugShare όταν το έργο είναι άδειο

erase flash and update firmware on ESP board
--MISSING--

Use board type
Χρησιμοποίηση κάρτας τύπου

Wiping board...
Διαγράφει την κάρτα…

(press ESC to cancel)
--MISSING--

Done!
Έγινε!

download and install latest VM
κατέβασε και εγκατάστησε το τελευταίο VM

Select board type:
Επιλογή τύπου κάρτας:

Uploading MicroBlocks to board...
Φορτώνει MicroBlocks στην κάρτα…

copy data to clipboard
αντίγραψε δεδομένα στο πρόχειρο

clear data
καθάρισε δεδομένα

clear memory and variables
καθάρισε την μνήμη και τις μεταβλητές

show advanced blocks
δείξε προχωρημένα blocks

export functions as library
εξαγωγή λειτουργιών σαν βιβλιοθήκη

hide advanced blocks
κρύψε προχωρημένα blocks

Data Graph
Γραφική Παράσταση

show instructions
δείξε εντολές

show compiled bytes
--MISSING--

expand
επέκτεινε

collapse
κατάρρευσε

rename...
μετονομασία

show block definition...
δείξε τον ορισμό του μπλοκ

show the definition of this block
δείξε τον ορισμό αυτού του μπλοκ

delete block definition...
διαγραφή ορισμού μπλοκ

delete the definition of this block
διαγραφή του ορισμού αυτού του μπλοκ

duplicate
αντίγραψε

duplicate this block
αντίγραψε αυτό το μπλοκ

delete block
διάγραψε το μπλοκ

delete this block
διάγραψε αυτό το μπλοκ

just this one block
μόνο αυτό το μπλοκ

copy to clipboard
αντίγραψε στο πρόχειρο

copy these blocks to the clipboard
αντίγραψε αυτά τα μπλοκ στο πρόχειρο

duplicate all
αντιγραφή όλων

duplicate these blocks
αντίγραψε αυτά τα μπλοκ

extract block
--MISSING--

pull out this block
τράβηξε έξω αυτό το μπλοκ

save picture of script
αποθήκευσε φωτογραφία του κειμένου

save a picture of this block definition as a PNG file
αποθήκευσε τον ορισμό αυτού του μπλοκ σε φωτογραφία σαν PNG  αρχείο

save a picture of these blocks as a PNG file
αποθήκευσε φωτογραφία αυτών των μπλοκ σαν PNG  αρχείο

copy script
αντιγραφή κειμένου

delete
διαγραφή

Input type:
Τύπος Εισόδου:

string only
μόνο συμβολοσειρά

string or number
συμβολοσειρά ή αριθμός

number only
αριθμός μόνο

define
όρισε

number/string
αριθμός/συμβολοσειρά

editable number or string
επεξεργάσιμος αριθμός ή συμβολοσειρά

label
ετικέτα

input
είσοδος

hide block definition
κρύψε τον ορισμό μπλοκ

Are you sure you want to remove this block definition?
Είσαι σίγουρος ότι επιθυμείς να αφαιρέσεις τον ορισμό του μπλοκ;

Language
Γλώσσα

Custom...
Προσαρμοσμένο…

Obsolete
Ξεπερασμένο

OK
--MISSING--

Ok
--MISSING--

Yes
Ναι

No
Όχι

Cancel
Ακύρωση

Okay
--MISSING--

Confirm
Επιβεβαίωση

# File picker and library dialogs

Libraries
Βιβλιοθήκες

Examples
Παραδείγματα

Desktop
Οθόνη

Computer
Υπολογιστής

Cloud
Νέφος

File
Αρχείο

File Open
Αρχείο Ανοιγμα

File Save
Αρχείο Αποθήκευση

File name:
Αρχείο όνομα:

New Folder
Νέος Φάκελος

by
από

Depends:
Εξαρτάται:

Tags:
Ετικέτες:

Path, name or URL for library?
Μονοπάτι, όνομα ή URL για βιβλιοθήκη;

Invalid URL
‘Aκυρο URL

Could not fetch library.
Δεν μπόρεσε να φορτώσει την βιβλιοθήκη.

Host does not exist or is currently down.
Οικοδεσπότης δεν υπάρχει ή δεν λειτουργεί

File not found in server.
Το αρχείο δεν βρέθηκε στον εξυπηρετητή.

Server expects HTTPS, and MicroBlocks doesn't currently support it.
Εξυπηρετητής  αναμένει HTTPS,  αλλά  δεν υποστηρίζεται από το MicroBlocks

library information
πληροφορία για την βιβλιοθήκη

built-in library
ενσωματωμένη βιβλιοθήκη

Dependency path, name or URL?
Εξαρτωμένη διαδρομή, όνομα ή URL;

Tag name?
Όνομα Ετικέτας;

